attribute vec2 a_position;
attribute vec2 a_texCoord;
uniform vec2 u_resolution;
varying vec2 v_texCoord;

void main() {
   vec2 zeroToOne = a_position / u_resolution; // pixels to 0.0 to 1.0
   vec2 zeroToTwo = zeroToOne * 2.0; // from 0->1 to 0->2
   vec2 clipSpace = zeroToTwo - 1.0; // 0->2 to -1->+1 (clipspace)

   gl_Position = vec4(clipSpace * vec2(1, -1), 0, 1);
   v_texCoord = a_texCoord; // passes the texCoord to fragment shader
}
